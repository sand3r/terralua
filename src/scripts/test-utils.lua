local M = {}

function countParams(args, requiredAmount)
  if #args ~= requiredAmount then
   print("Missing arguments.");
   help();
   return 0
 end
end

M.countParams = countParams;

return M;
