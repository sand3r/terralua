-- x
-- xx
-- xxx
-- xxx
-- Txx
-- sxx
-- fsx
--  fs
--   f

-- Stairs in slot 1
-- Foundation in slot 16 (commented out)


local ips = require("ips")
local utils = require("utils")


function doColumn()
  for i=1,3 do
    utils.clearDirection("up")
    ips.moveUp()
  end

  for i=1,3 do
    ips.moveDown()
  end

  for i=1,2 do
    utils.clearDirection("down")
    ips.moveDown()
  end

  -- Foundation
  ips.moveUp()
  -- turtle.selectSlot(16)
  -- turtle.placeDown()

  -- Stair
  ips.moveUp()
  turtle.selectSlot(1)
  turtle.placeDown()
end

function createStairs()

  for i=1,5 do
    doColumn()
    clearDirection('forward')
    ips.moveForward()
    turtle.digDown()
    ips.moveDown()
  end

end


