-- btf

local args = {...}
os.loadAPI('terralua/ips')
os.loadAPI('terralua/utils')

-- Config
if args[1] ~= nil and tonumber(args[1]) ~= nil then
  amount = tonumber(args[1])
else
  amount = 1
end

-- Definition

function digBasin()
  shell.run('dr', '7', '3', '2', '0');
end

function digMachineRoom()
  shell.run('dr', '7', '7', '3', '0');
end

function digUnit()
  -- Dig left basin
  digBasin()

  -- Move to right basin
  ips.moveForward(6)
  utils.clearDirection('down')
  ips.moveDown()
  ips.turnRight()
  ips.moveForward(4)

  -- Arrived in other 'to be' basin
  ips.turnRight()
  ips.moveForward(6)
  ips.turn180()
  utils.clearDirection('up')
  ips.moveUp()

  -- Dig right basin
  digBasin()

  -- Move to 'to be' machine area, below position of starting position
  utils.clearDirection('down')
  ips.moveDown()
  ips.moveForward(6)
  ips.turnLeft()
  ips.moveForward()
  ips.turnLeft() -- Ready to sink

  for i=1, 3 do
    utils.clearDirection('down')
    ips.moveDown()
  end

  -- Move under tree girder
  ips.moveForward(6)
  ips.turnRight()
  ips.moveForward(3)

  ips.turnRight()
  digMachineRoom()

  -- Move back to the starting position
  ips.moveForward(6)
  ips.turnRight()
  ips.moveForward(3)
  ips.turn180()
  for i=1, 3 do
    utils.clearDirection('up')
    ips.moveUp()
  end
  ips.moveForward(3)
  ips.turnLeft()
  ips.moveForward(6)
  ips.turn180()
  utils.clearDirection('up')
  ips.moveUp()
end

function buildFarm(amount)

  print('Building a Farm with ' .. amount .. ' unit(s)')

  for i=1,amount do
    digUnit()

    if i < amount then
      -- Move to next Unit position
      ips.moveUp()
      ips.moveForward(8)
      ips.moveDown()
    end

  end

end


-- Execution
buildFarm(amount)
