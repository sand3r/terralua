-- Server to wait for redstone input signal and run wp
-- print("Waiting for start time of 18000...");


--
-- Load Config
--
os.loadAPI('terralua/scripts/utils')
local baseConfig = {
  startTime = 20000
}
appConfig = utils.loadConfig('wp_on_time', baseConfig);

--
-- Execution
--
print("Waiting for start time of " .. appConfig.startTime);
print("To abort: hold CTRL+T");

while true do
  local dayTime = os.time() * 1000;
  print("Current time: " .. dayTime)
  -- if dayTime > 18000 and dayTime < 18400 then
  if dayTime > appConfig.startTime and dayTime < (appConfig.startTime + 300) then
    shell.run("wp")
    shell.run("wp_on_time")
    break;
  end
  sleep(10);
end
