-- ut = update test
--
-- Only purpose of this file is to easily update test.lua

fs.delete("terralua/test")
response = http.get("https://bitbucket.org/sand3r/terralua/raw/master/src/test.lua")
body = response.readAll()
print(body)
handle = fs.open("terralua/test", "w")
handle.write(body)
handle.close()
