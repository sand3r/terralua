-- Woodpecker Script, created by Sander.
-- Script that can be used for turtles to let them dig down a row of trees
-- where the trunksize can be specified for either 1x1 or 2x2.
--
-- Usage: wp [amount of trees, default 4] [trunksize, default 1]
--
--
-- Farm Setup (4 tree setup, as seen from above)
--
-- /-\
-- | t
-- | .
-- | t
-- | .
-- | t
-- | .
-- | t
-- | .
-- BBT
--  BC
--
-- Legend:
-- T = Turtle
-- B = Block (required to guide the turtle)
-- . = space between trees (minimum space is 1)
-- t = tree
-- C = Chest
-- | = Path the turtle will follow after chopping down the trees

os.loadAPI('terralua/scripts/ips')
os.loadAPI('terralua/scripts/utils')

local args = {...}
local height = 0

-- Reserved slots
local SLOT_BONEMEAL = 14
local SLOT_SAPLINGS = 15
local SLOT_FUEL = 16

-- Do some checks before departure
function preCheck()

  local required_saplings = 0

  -- check saplings
  required_saplings = appConfig.treesPerRow

  if turtle.getItemCount(SLOT_SAPLINGS) < required_saplings then
    print("Not enough saplings to complete run")
    return false
  end

  -- check fuel
  if turtle.getFuelLevel() < 1000 then
    print("Turtle is almost out of fuel. Halting. ( < 1000)")
    return false
  else
    print("Fuel Level: " .. turtle.getFuelLevel())
  end

  -- ready to go
  print("-- Precheck passed, off we go!")
  return true

end

function refuel()

  if turtle.getItemCount(SLOT_FUEL) > 0 then
    turtle.selectSlot(SLOT_FUEL)
    turtle.refuel()
  end

end

function cutWoods()

  for i=1, appConfig.rows do

    cutRow(appConfig.treesPerRow, appConfig.trunkSize)

    if i == appConfig.rows then
      print("End of rows.")
      moveBackToBase()
    else
      moveToNextRow()
    end

  end

end


-- Move the turtle back to the original parking spot
-- to unload
function moveBackToBase()

  ips.turnLeft()
  ips.moveForward()
  ips.moveForward()
  ips.turnLeft()

  while true do
    if turtle.detect() then
      -- base detected
      turtle.turnLeft()
      turtle.forward()
      turtle.forward()
      turtle.turnRight()
      sleep(3) -- debug
      turtle.forward()
      unload()
      turn180()
      break
    else
      turtle.forward()
    end
  end

  sleep(3); -- debug
  loadSaplings();
end

function loadSaplings()
  ips.moveUp()

  ips.turnLeft();

  if turtle.detect() then
    turtle.select(SLOT_SAPLINGS)
    local saplingCountToLoad = 64 - turtle.getItemCount();

    print('Loading saplings.');
    print(saplingCountToLoad);
    turtle.suck(saplingCountToLoad);
    turtle.select(1)
  else
    print('No inventory detected to load saplings.');
  end

  ips.turnRight();
  ips.moveDown();
end


function moveToNextRow()

  ips.moveUp()
  ips.turnRight()
  ips.moveForward()
  ips.moveForward()
  ips.turnRight()
  ips.moveForward()
  ips.turnLeft()

  while true do

    if turtle.detect() then
      break
    else
      ips.moveForward()
    end

  end

  ips.turnLeft()
  ips.moveForward()
  ips.moveDown()

end

function cutRow(treeCount, trunkSize)

  for i=1,treeCount do
    while true do

      if turtle.detect() then
        -- tree detected
        break
      else
        turtle.forward()
      end
    end

    cutTree(trunkSize)
    turtle.forward()
    turn180()
    plantSaplings(trunkSize)
    turn180()
  end
end

-- @param   trunksize     length x width
function cutTree(trunkSize)

  if trunkSize == 1 then
    -- get under the tree
    turtle.dig()
    turtle.forward()
    cutVertical()

  elseif trunkSize == 2 then
    -- Map:
    -- AB
    -- CD
    --  ▲ (Turtle approaches from this side)

    -- D
    turtle.dig()
    turtle.forward()
    cutVertical()

    -- C
    turtle.turnLeft()
    turtle.dig()
    turtle.forward()
    turtle.turnRight()
    cutVertical()

    -- A
    turtle.dig()
    turtle.forward()
    cutVertical()

    -- B
    turtle.turnRight()
    turtle.dig()
    turtle.forward()
    turtle.turnLeft()
    cutVertical()
  end

end

function cutVertical()

  local height = 0

  while true do
    if turtle.detectUp() then
      turtle.digUp()
      turtle.up()
      height = height + 1
    else
      break
    end
  end

  for i=1, height do
    turtle.down()
  end
end

function plantSaplings(trunkSize)

  turtle.select(SLOT_SAPLINGS)

  if trunkSize == 2 then
    -- Map:
    --  ▼ (Turtle approaches from this side)
    -- AB
    -- CD

    -- D
    turtle.forward()
    turtle.place()

    -- C
    turtle.turnRight()
    turtle.forward()
    turtle.turnLeft()
    turtle.place()

    -- A
    turtle.turnRight()
    turtle.back()
    turtle.place()

    -- D
    turtle.turnLeft()
    turtle.back()
    turtle.place()

  else
    turtle.place()
  end


  -- bonemeal
  turtle.select(SLOT_BONEMEAL)
  if turtle.getItemCount(SLOT_BONEMEAL) > 0 then
    turtle.place()
  end

  turtle.select(1)
end

function backToBase()
  turtle.turnLeft()
  turtle.forward()
  turtle.forward()
  turtle.turnLeft()

  while true do

    if turtle.detect() then
      -- base detected
      turtle.turnLeft()
      turtle.forward()
      turtle.forward()
      turtle.turnRight()
      turtle.forward()
      unload()
      turn180()
      break
    else
      turtle.forward()
    end

  end

end

function turn180()
  turtle.turnRight()
  turtle.turnRight()
end

function unload()
  -- unload slots 1 to 10
  for i=1,10 do
    turtle.select(i)
    turtle.drop()
    sleep(1) -- required to let the turtle unload properly.
  end
end

function report()
  print("Done. Remaining fuel level:")
  print(turtle.getFuelLevel())
end

--
-- Load Config
--

local baseConfig = {
  rows = 1,
  treesPerRow = 1,
  trunkSize = 1       -- 1 = trunk is 1x1, 2 = trunk is 2x2
}

appConfig = utils.loadConfig('wp', baseConfig);
print('Config:')
print("Amount of rows: " .. appConfig.rows)
print("Trees per row: " .. appConfig.treesPerRow)
print("Trunksize: " .. appConfig.trunkSize)


--
-- Program Start
--
if preCheck() then
  cutWoods()
  turtle.select(1)
end

report()
