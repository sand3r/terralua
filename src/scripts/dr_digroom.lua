-- Dig Room
-- Turtle Requirements: Turtle with pickaxe (modem is no longer required)
--
-- Usage:
-- 1. Load fuel (1000 is minimum)
--
-- 2. Place turtle at T (as seen from above):
--    d d d d d
--    d d d d d
--    d d d d d
--    d d d d d
--    T d d d d
--    C
--
-- 3. Place chest at C (as seen from above):
--
-- 4. Run:
--    See help()

-- POSITIONING SYSTEM (@workinprogress)
-- The turtle uses IPS (Independent Positioning System), which allows it to
-- return back to the originating position.

--
-- GLOBALS
--
local args = {...}
os.loadAPI('terralua/scripts/ips')
os.loadAPI('terralua/scripts/utils')

--
-- METHODS
--

-- Return the count of parameters.
function countParams()
   if #args ~= 3 then
    print("Missing arguments.")
    help()
    return 0
  end
end

function message(message)
  -- rednet.send(35, tostring(message))
end

function help()
  print('Run using l w h [0 = downwards (default), 1 = upwards]')
end

function digRoom(length, width, height, inverted)

  inverted = inverted or 0

  print('inverted is: ' .. inverted)

  for w=1, width do

    if w>1 then
      ips.shiftRight()
    end

    for h=1,height do
      digRow(length)

      if inverted == 1 then
        if h < height then
          utils.clearDirection("up")
          turtle.up()
        end
      else
        if h < height then
          utils.clearDirection("down")
          turtle.down()
        end
      end

    end

    -- Move the turtle back
    for h=1, (height - 1) do
      if inverted == 1 then
        turtle.down()
      else
        utils.clearDirection("up")
        turtle.up()
      end
    end

  end

end

-- Dig a row, with depth and return to start position when done.
function digRow(length)
  local l = length - 1
  -- Dig
  ips.moveForward(l)

  -- Return to beginning
  ips.turn180()

  ips.moveForward(l)

  ips.turn180()
end


function hasTurtleSlotsAvailable()
  local maxSlots = 16
  local emptySlotAvailable = false

  for s=1, maxSlots do
    if turtle.getItemCount(s) == 0 then
      emptySlotAvailable = true
      break
    end
  end

  return emptySlotAvailable

end

--
-- PRECHECK
--
countParams();

--
-- PREPARATION
--
message("Turtle started.")
message(tostring(os.clock()))
message("Turtle connected")
message("Start fuel level:")
message(turtle.getFuelLevel())

-- Argument parsing and defaults
args[1] = tonumber(args[1]) -- l
args[2] = tonumber(args[2]) -- w
args[3] = tonumber(args[3]) -- h
args[4] = tonumber(args[4]) -- bottom/top (0 = bottom oriented, 1 = top oriented)

--
-- EXECUTION
--
message("Fuel available: " .. tostring(turtle.getFuelLevel()));
digRoom(args[1], args[2], args[3], args[4])

-- Return to start position
ips.turnLeft()
for i=2,args[2] do
  ips.moveForward()
end
ips.turnRight()

-- rednet.close("right")
message("Done")
message("Fuel left:");
message(tostring(turtle.getFuelLevel()))
