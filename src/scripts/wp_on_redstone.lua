-- Server to wait for redstone input signal and run wp
print("Waiting for redstone signal input on right side.");
print("To abort: hold CTRL+T");

while true do
  os.pullEvent("redstone")
  if redstone.getInput("right") then
    shell.run("wp")
    shell.run("wp_on_redstone")
    break;
  end
end
