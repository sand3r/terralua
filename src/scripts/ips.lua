--
-- Independent Positioning System for ComputerCraft Turtles
--
local offsetY = 0       -- up/down
local offsetX = 0       -- left/right
local offsetZ = 0       -- forward/backward
local orientation = 0

os.loadAPI('terralua/scripts/utils')

--
-- MOVEMENT
--
function getPosition()
  return "Z:" .. offsetZ .. " X:" .. offsetX .. " Y:" .. offsetY
end

-- moveForward 'blocks' times
function moveForward(blocks)

  blocks = blocks or 1

  for i=1, blocks do

    local movedForward = false

    while not movedForward do
      if turtle.forward() then
        movedForward = true
        break
      else
        turtle.dig()
        turtle.attack()
      end
    end

  end

end

function moveBackward()

  local movedBackward = false

  while not movedBackward do
    if turtle.back() then
      movedBackward = true
      break
    else
      turn180()
      turtle.dig()
      turtle.attack()
      turn180()
    end
  end

end

function moveUp()
  if turtle.up() then
    offsetZ = offsetZ + 1;
  else
    print("Turtle couldn't move up");
  end
end

function moveDown()
  if turtle.down() then
    offsetZ = offsetZ - 1;
  else
    print("Turtle couldn't move down");
  end
end

-- Move the turtle to the left, while after moving facing the same position
function shiftLeft()
  turtle.turnLeft()
  utils.clearDirection("forward")
  if not turtle.forward() then
    print("E: Couldnt move Fwd at 114")
  end  turtle.turnRight()
end

-- Move the turtle to the right, while after moving facing the same position
function shiftRight()
  turtle.turnRight()
  utils.clearDirection("forward")
  if not turtle.forward() then
    print("E: Couldnt move Fwd at 123")
  end
  turtle.turnLeft()
end

function turnLeft()
  turtle.turnLeft();
  if(orientation > 0) then
    orientation = orientation - 1;
  else
    orientation = 0
  end
end

function turnRight()
  turtle.turnRight();
  if(orientation < 4) then
    orientation = orientation + 1;
  else
    orientation = 4
  end
end

-- Rotate the turtle 180 degrees
function turn180()
  turnRight()
  turnRight()
end

function updatePosition(direction)
  if direction == 'forward' then
    offsetZ = offsetZ + 1
  elseif direction == 'backward' then
    offsetZ = offsetZ - 1
  end
end
