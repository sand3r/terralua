function joinTables(t1, t2)
  for key,value in pairs(t1)
  do
    if type(t2[key]) ~= 'nil' then
      print('merging')
      t1[key] = t2[key]
    else
      print('not merging')
      print(t2[key])
    end
  end
  return t1
end

appConfig = {
  rows = false,

}

baseAppConfig = {
  rows = 8,
  columns = 8
}

config = joinTables(baseAppConfig, appConfig);

print(config['rows'])
print(config['columns'])
