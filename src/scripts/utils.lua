-- Clears the position in "orientation" by digging and waits for falling blocks
-- before completing the sequence.

local sleep_time = 0.5

function clearDirection(orientation)

  while true do

    if orientation == "forward" then
      if turtle.detect() then
        turtle.dig()
        sleep(sleep_time)
      else
        break
      end
    elseif orientation == "up" then
      if turtle.detectUp() then
        turtle.digUp()
        sleep(sleep_time)
      else
        break
      end
    elseif orientation == "down" then
      if turtle.detectDown() then
        turtle.digDown()
      else
        break
      end
    end

  end

end

function mergeTables(baseConfig, userConfig)
  for key,value in pairs(baseConfig) do
    if type(userConfig[key]) ~= 'nil' then
      baseConfig[key] = userConfig[key]
    end
  end
  return baseConfig
end

function loadConfig(appName, baseConfig)
  if fs.exists('tlconfig/' .. appName) then
    print("Config file found.")

    local handle = fs.open('tlconfig/' .. appName, 'r')
    local data = handle.readAll()
    print("Config data")
    print(data);
    local userConfig = textutils.unserialize(data)
    print(userConfig);
    handle.close()

    baseConfig = mergeTables(baseConfig, userConfig)
  else
    print("Config file not found.")
  end
  return baseConfig
end

function getRandomCacheKey()
  print("getRandomCacheKey time: " .. os.time());
  math.randomseed(os.time())
  return math.random(10,100001)
end
