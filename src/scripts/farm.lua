-- Manages a l x w farm.
-- Usage: run 'farm' everytime you want to farm your land and plant new
-- seeds (taken from slot 1).

local args = {...}

function farm(length, width)

  for i=1,width do
    farmRow(length)
    
    if(i == width) then
      -- Last sequence
      turtle.digDown()
    else
      turtle.turnRight()
      turtle.forward()
      turtle.turnLeft()
    end
  end

  -- Return to lateral start position
  turtle.turnLeft()
  for m=1,(width - 1) do
    turtle.forward()
  end
  turtle.turnRight()

end

function farmRow(length)
  
  -- Move to the back of the row
  for i=0,(length - 1) do
    turtle.dig()
    turtle.forward()
  end
  
  -- Till the dirt
  for i=0,(length - 1) do
    turtle.back()
    turtle.dig()
    turtle.place()
  end
  
end

-- Globals... On Purpose.
length = 16
width = 4

farm(length, width)
