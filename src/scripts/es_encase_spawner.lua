function moveToNext()
  -- Move to next
  turtle.turnLeft()
  turtle.forward()
  turtle.forward()
  turtle.turnRight()
  turtle.forward()
  turtle.forward()
  turtle.turnRight()
end

-- Move to spawner
while not turtle.detect() do
  turtle.forward()
end

turtle.back()

-- 1st candle
turtle.place()
moveToNext()

-- 2nd candle
turtle.place()
moveToNext()

-- 3rd candle
turtle.place()
moveToNext()

-- 4th candle
turtle.place()

-- Move to top
turtle.up()
turtle.up()
turtle.forward()
turtle.forward()

-- top candle
turtle.placeDown()

-- base
turtle.turnRight()
turtle.back()
turtle.back()
turtle.down()
turtle.down()
turtle.turnLeft();
turtle.turnLeft();

-- backwards to safety
while not turtle.detect() do
  turtle.forward()
end

