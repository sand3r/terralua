local args = {...}
current_slot = 1

-- Pre-depature checks
function preCheck()
  local inventory_count = 0

  inventory_count = countTotalBlocks()
  
  if (inventory_count < (length * width * height)) then
    print('Not enough resources in inventory to complete run!')
    return false;
  end

  if(turtle.getFuelLevel() < (length * width)) then
    print('Not enough fuel to complete run!')
    return false;
  end
  
  return true;

end

-- Select the next occupied slot that contains items to fill
function selectNextOccupiedSlot(offset)
end

-- Count total blocks in the inventory
function countTotalBlocks()
  local inventory_count = 0

  for i=1,16 do
    inventory_count = inventory_count + turtle.getItemCount(i)
  end  

  return inventory_count

end

function fillRow(length)
  
  -- Move to the back of the row
  for i=0,(length - 2) do
    turtle.forward()
  end
  
  -- Place the blocks
  for i=0,(length - 2) do
    turtle.back()
    if(turtle.getItemCount(current_slot) == 0) then
      turtle.select(current_slot + 1);
      current_slot = current_slot + 1;
    end
    turtle.place()
  end
  
end

function fill(length, width, height)

  for h=1,height do

    for i=1,width do
      fillRow(length)
      
      if(i == width) then
        -- Last sequence
        turtle.up()
        turtle.placeDown()
      else
        turtle.turnRight()
        turtle.forward()
        turtle.turnLeft()
        turtle.turnLeft()
        turtle.place()
        turtle.turnRight()
      end
    end

    -- Return to lateral start position
    turtle.turnLeft()
    for m=1,(width - 1) do
      turtle.forward()
    end
    turtle.turnRight()

  end

end

if args[3] == nil then
  args[3] = 1
end

args[1] = tonumber(args[1])
args[2] = tonumber(args[2])
args[3] = tonumber(args[3])

-- Globals... On Purpose.
length = args[1]
width = args[2]
height = args[3]

if preCheck() then
  fill(length, width, height)
end

turtle.select(1);
