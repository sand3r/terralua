print("Running the updater, v1.0.10");

math.randomseed(os.time())
local cacheInt = math.random(10,100001)
print("Using cacheInt: " .. cacheInt);

--
-- TerraLua Basics
--
response = http.get("https://bitbucket.org/sand3r/terralua/raw/master/src/update.lua?" .. cacheInt)
body = response.readAll()
-- print(body)
handle = fs.open("terralua/update", "w")
handle.write(body)
handle.close()

response = http.get("https://bitbucket.org/sand3r/terralua/raw/master/src/upgrade.lua?" .. cacheInt)
body = response.readAll()
-- print(body)
handle = fs.open("terralua/upgrade", "w")
handle.write(body)
handle.close()


--
-- Essentials/dependencies
--
response = http.get("https://bitbucket.org/sand3r/terralua/raw/master/src/scripts/ips.lua?" .. cacheInt)
body = response.readAll()
-- print(body)
handle = fs.open("terralua/scripts/ips", "w")
handle.write(body)
handle.close()

response = http.get("https://bitbucket.org/sand3r/terralua/raw/master/src/scripts/utils.lua?" .. cacheInt)
body = response.readAll()
-- print(body)
handle = fs.open("terralua/scripts/utils", "w")
handle.write(body)
handle.close()



--
-- Functions/Programs
--

-- Dig Room (dr)
response = http.get("https://bitbucket.org/sand3r/terralua/raw/master/src/scripts/dr_digroom.lua?" .. cacheInt)
body = response.readAll()
-- print(body)
handle = fs.open("terralua/scripts/dr", "w")
handle.write(body)
handle.close()

-- Woodpecker (wp)
response = http.get("https://bitbucket.org/sand3r/terralua/raw/master/src/scripts/wp_woodpecker.lua?" .. cacheInt)
body = response.readAll()
-- print(body)
handle = fs.open("terralua/scripts/wp", "w")
handle.write(body)
handle.close()

--
-- Functions/Programs
--

-- Update (shortcut)
response = http.get("https://bitbucket.org/sand3r/terralua/raw/master/src/scripts/update.lua?" .. cacheInt)
body = response.readAll()
-- print(body)
handle = fs.open("terralua/scripts/update", "w")
handle.write(body)
handle.close()

-- Test Utils
response = http.get("https://bitbucket.org/sand3r/terralua/raw/master/src/scripts/test.lua?" .. cacheInt)
body = response.readAll()
-- print(body)
handle = fs.open("terralua/scripts/test", "w")
handle.write(body)
handle.close()

-- Test Utils
response = http.get("https://bitbucket.org/sand3r/terralua/raw/master/src/scripts/test_script.lua?" .. cacheInt)
body = response.readAll()
-- print(body)
handle = fs.open("terralua/scripts/test_script", "w")
handle.write(body)
handle.close()

response = http.get("https://bitbucket.org/sand3r/terralua/raw/master/src/scripts/test_string.lua?" .. cacheInt)
body = response.readAll()
-- print(body)
handle = fs.open("terralua/scripts/test_string", "w")
handle.write(body)
handle.close()

response = http.get("https://bitbucket.org/sand3r/terralua/raw/master/src/scripts/wp_on_redstone.lua?" .. cacheInt)
body = response.readAll()
-- print(body)
handle = fs.open("terralua/scripts/wp_on_redstone", "w")
handle.write(body)
handle.close()

response = http.get("https://bitbucket.org/sand3r/terralua/raw/master/src/scripts/wp_on_time.lua?" .. cacheInt)
body = response.readAll()
-- print(body)
handle = fs.open("terralua/scripts/wp_on_time", "w")
handle.write(body)
handle.close()

--
-- Turtle Configuration
--
handle = fs.open("./startup", "w")
handle.write('shell.setDir("terralua/scripts")')
handle.close()

shell.setDir("terralua/scripts")
